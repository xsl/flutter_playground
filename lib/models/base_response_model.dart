class BaseResponseModel {
  late final int status;
  late final String message;
  late final dynamic result;

  BaseResponseModel({
    required this.status,
    required this.message,
  });

  factory BaseResponseModel.fromJson(Map<String, dynamic> data) {
    final status = data['status'] as int;
    final message = data['message'] as String;
    final result = data['result'];
    return BaseResponseModel(status: status, message: message);
  }
}
