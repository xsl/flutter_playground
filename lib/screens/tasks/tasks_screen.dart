import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:go_router/go_router.dart';

class TasksScreen extends StatefulWidget {
  const TasksScreen({super.key});

  @override
  State<TasksScreen> createState() => _TasksScreenState();
}

class _TasksScreenState extends State<TasksScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text('Tasks'),
        leading: InkWell(
          child: const Icon(
            FontAwesome.left,
          ),
          onTap: () => context.go('/'),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => {
              context.go('/tasks/create'),
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          Image.network(
            'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png',
            width: 1000,
          ),
          ElevatedButton(
            onPressed: () => {
              context.go('/'),
            },
            child: const Text('Go Home'),
          )
        ],
      ),
    );
  }
}
