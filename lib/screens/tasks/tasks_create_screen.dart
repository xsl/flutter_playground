import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:go_router/go_router.dart';

class TasksCreateScreen extends StatefulWidget {
  const TasksCreateScreen({super.key});

  @override
  State<TasksCreateScreen> createState() => _TasksCreateScreenState();
}

class _TasksCreateScreenState extends State<TasksCreateScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text('Create new Task'),
        leading: InkWell(
          child: const Icon(
            FontAwesome.left,
          ),
          onTap: () => context.go('/'),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.fork_left),
            onPressed: () => {
              context.go('/tasks'),
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          Image.network(
            'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png',
            width: 1000,
          ),
          ElevatedButton(
            onPressed: () => {
              context.go('/'),
            },
            child: const Text('Go Home'),
          )
        ],
      ),
    );
  }
}
