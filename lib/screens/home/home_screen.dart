import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:fluttericon/linecons_icons.dart';
import 'package:fluttericon/typicons_icons.dart';

import '../../components/drawer_primary_component.dart';
import '../../constants.dart';
import '../pandoras/webview_screen.dart';
import '../settings/settings_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String _currentCompanyId = 'one';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 5,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: bPrimaryColor,
            centerTitle: true,
            elevation: 1.0,
            // leading: const Icon(FontAwesome.user),
            leading: IconButton(
              icon: const Icon(Linecons.user),
              onPressed: () {
                _scaffoldKey.currentState!.openDrawer();
              },
            ),
            title: Theme(
              data: ThemeData.dark(),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  value: _currentCompanyId,
                  dropdownColor: bPrimaryColor,
                  items: const <DropdownMenuItem<String>>[
                    DropdownMenuItem(
                      value: 'one',
                      child: Text('OpenAI Inc.'),
                    ),
                    DropdownMenuItem(
                      value: 'two',
                      child: Text('SenseTime Inc.'),
                    ),
                    DropdownMenuItem(
                      value: 'three',
                      child: Text('Google Inc.'),
                    ),
                    DropdownMenuItem(
                      value: 'four',
                      child: Text('BLM Inc.'),
                    ),
                    DropdownMenuItem(
                      value: 'five',
                      child: Text('Baidu Inc.'),
                    ),
                    DropdownMenuItem(
                      value: 'six',
                      child: Text('Sina Inc.'),
                    ),
                    DropdownMenuItem(
                      value: 'seven',
                      child: Text('Weibo Inc.'),
                    ),
                    DropdownMenuItem(
                      value: 'eight',
                      child: Text('Tencent Inc.'),
                    ),
                    DropdownMenuItem(
                      value: 'nine',
                      child: Text('Alibaba Inc.'),
                    ),
                  ],
                  onChanged: (value) {
                    setState(() {
                      _currentCompanyId = value!;
                    });
                  },
                ),
              ),
            ),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.more_vert),
                onPressed: () => {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const SettingsScreen(),
                    ),
                  )
                },
              ),
            ],
          ),
          bottomNavigationBar: Container(
            color: bPrimaryColor,
            child: const SafeArea(
              child: TabBar(
                indicatorColor: Colors.white,
                tabs: <Tab>[
                  Tab(
                    icon: Icon(
                      Typicons.home,
                      size: 20.0,
                    ),
                    text: 'Home',
                  ),
                  Tab(
                    icon: Icon(
                      FontAwesome.list,
                      size: 20.0,
                    ),
                    text: 'Tasks',
                  ),
                  Tab(
                    icon: Icon(
                      Typicons.th_large_outline,
                      size: 20.0,
                    ),
                    text: 'Apps',
                  ),
                  Tab(
                    icon: Icon(
                      Entypo.user,
                      size: 20.0,
                    ),
                    text: 'Contacts',
                  ),
                  Tab(
                    icon: Icon(
                      Typicons.wrench_outline,
                      size: 20.0,
                    ),
                    text: 'Settings',
                  ),
                ],
              ),
            ),
          ),
          body: const TabBarView(
            children: [
              Icon(Icons.directions_car),
              Icon(Icons.directions_transit),
              Icon(Icons.directions_bike),
              Icon(Icons.directions_transit),
              Icon(Icons.directions_bike),
            ],
          ),
          drawer: const DrawerPrimaryComponent(),
          floatingActionButton: FloatingActionButton(
            backgroundColor: bPrimaryColor,
            onPressed: () {
              // debugPrint('FAB Clicked');
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const WebViewScreen(
                  title: 'mihoyo@Gitee',
                  url: 'https://gitee.com/mihoyo',
                ),
              ));
            },
            tooltip: '报修',
            // child: const Icon(FontAwesome.bug),
            child: SvgPicture.asset(
              'assets/icons/fix_report.svg',
              colorFilter: const ColorFilter.mode(
                Colors.white,
                BlendMode.srcIn,
              ),
              width: 24.0,
              height: 24.0,
              fit: BoxFit.scaleDown,
            ),
          ), // This trailing comma makes auto-formatting nicer for build methods.
        ),
      ),
    );
  }
}
