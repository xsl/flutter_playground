import 'package:flutter/material.dart';

class AppsScreen extends StatefulWidget {
  const AppsScreen({super.key});

  @override
  State<AppsScreen> createState() => _AppsScreenState();
}

class _AppsScreenState extends State<AppsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(0),
            child: InkWell(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(0),
                child: Image.network(
                  'http://pre.blmfm.cn/static/img/background.40dc8b15.png',
                  width: 1000,
                ),
              ),
              onTap: () => {
                debugPrint('onTap'),
              },
            ),
          ),
        ],
      ),
    );
  }
}
