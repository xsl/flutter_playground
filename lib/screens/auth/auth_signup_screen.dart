import 'package:blmapp/fragments/auth_signup_form.dart';
import 'package:flutter/material.dart';

import '../../components/background.dart';
import '../../constants.dart';
import '../../fragments/auth_signup_screen_top_image.dart';
import '../../responsive.dart';

class AuthSignupScreen extends StatelessWidget {
  const AuthSignupScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Background(
      child: SingleChildScrollView(
        child: Responsive(
          mobile: const MobileAuthSignupScreen(),
          desktop: Row(
            children: [
              const Expanded(
                child: AuthSignupScreenTopImage(),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SizedBox(
                      width: 450,
                      child: AuthSignupScreenTopImage(),
                    ),
                    SizedBox(
                      height: defaultPadding / 2,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MobileAuthSignupScreen extends StatelessWidget {
  const MobileAuthSignupScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const AuthSignupScreenTopImage(),
        Row(
          children: const [
            Spacer(),
            Expanded(
              flex: 8,
              child: AuthSignupForm(),
            ),
            Spacer(),
          ],
        ),
        // const SocalSignUp()
      ],
    );
  }
}
