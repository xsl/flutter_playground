import 'package:flutter/material.dart';

import '../../components/background.dart';
import '../../fragments/auth_login_form.dart';
import '../../fragments/auth_login_screen_top_image.dart';
import '../../responsive.dart';

class AuthLoginScreen extends StatelessWidget {
  const AuthLoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Background(
      child: SingleChildScrollView(
        child: Responsive(
          mobile: const MobileAuthLoginScreen(),
          desktop: Row(
            children: [
              const Expanded(
                child: AuthLoginScreenTopImage(),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SizedBox(
                      width: 450,
                      child: AuthLoginForm(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MobileAuthLoginScreen extends StatelessWidget {
  const MobileAuthLoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const AuthLoginScreenTopImage(),
        Row(
          children: const [
            Spacer(),
            Expanded(
              flex: 8,
              child: AuthLoginForm(),
            ),
            Spacer(),
          ],
        ),
      ],
    );
  }
}
