import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../home/home_screen.dart';
import '../welcome/welcome_screen.dart';

final List<String> imgList = [
  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
];

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  int _currentSliderPageIndex = 0;

  final CarouselController _controller = CarouselController();

  final List<Widget> carouselImages = imgList
      .map((item) => Container(
            margin: const EdgeInsets.all(8.0),
            child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                child: Stack(
                  children: <Widget>[
                    Image.network(
                      item,
                      fit: BoxFit.cover,
                      width: 1000.0,
                    ),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 16.0),
                        child: Text(
                          'No. ${imgList.indexOf(item)} image',
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ))
      .toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
        backgroundColor: bPrimaryColor,
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        reverse: false,
        primary: true,
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            child: CarouselSlider(
              items: carouselImages,
              carouselController: _controller,
              options: CarouselOptions(
                initialPage: _currentSliderPageIndex,
                autoPlay: true,
                enlargeCenterPage: true,
                aspectRatio: 16 / 9,
                onPageChanged: (index, reason) => {
                  setState(
                    () => _currentSliderPageIndex = index,
                  )
                },
              ),
            ),
          ),
          const SizedBox(
            height: 8.0,
            child: DecoratedBox(
              decoration: BoxDecoration(
                color: Color(0xffefefef),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(defaultPadding),
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.add),
                        color: bPrimaryColor,
                        iconSize: 42,
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => const HomeScreen(),
                            ),
                          );
                        },
                      ),
                      const Text(
                        '自选基金',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: bPrimaryColor,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.safety_check),
                        color: Colors.teal,
                        iconSize: 42,
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => const WelcomeScreen(),
                            ),
                          );
                        },
                      ),
                      const Text(
                        '保险',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.card_giftcard),
                        color: Colors.amber,
                        iconSize: 42,
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => const WelcomeScreen(),
                            ),
                          );
                        },
                      ),
                      const Text(
                        '基金开户',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.amber,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.money),
                        color: Colors.cyan,
                        iconSize: 42,
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => const WelcomeScreen(),
                            ),
                          );
                        },
                      ),
                      const Text(
                        '稳健理财',
                        style: TextStyle(
                          color: Colors.cyan,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 8.0,
            child: DecoratedBox(
              decoration: BoxDecoration(
                color: Color(0xffefefef),
              ),
            ),
          ),
          ListView(
            padding: const EdgeInsets.all(8.0),
            scrollDirection: Axis.vertical,
            reverse: false,
            primary: true,
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            children: const <Widget>[
              ListTile(
                title: Text("Battery Full"),
                subtitle: Text("The battery is full."),
                leading: Icon(Icons.battery_full),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Anchor"),
                subtitle: Text("Lower the anchor."),
                leading: Icon(Icons.anchor),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Alarm"),
                subtitle: Text("This is the time."),
                leading: Icon(Icons.access_alarm),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Ballot"),
                subtitle: Text("Cast your vote."),
                leading: Icon(Icons.ballot),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Battery Full"),
                subtitle: Text("The battery is full."),
                leading: Icon(Icons.battery_full),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Anchor"),
                subtitle: Text("Lower the anchor."),
                leading: Icon(Icons.anchor),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Alarm"),
                subtitle: Text("This is the time."),
                leading: Icon(Icons.access_alarm),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Ballot"),
                subtitle: Text("Cast your vote."),
                leading: Icon(Icons.ballot),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Battery Full"),
                subtitle: Text("The battery is full."),
                leading: Icon(Icons.battery_full),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Anchor"),
                subtitle: Text("Lower the anchor."),
                leading: Icon(Icons.anchor),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Alarm"),
                subtitle: Text("This is the time."),
                leading: Icon(Icons.access_alarm),
                trailing: Icon(Icons.star),
              ),
              ListTile(
                title: Text("Ballot"),
                subtitle: Text("Cast your vote."),
                leading: Icon(Icons.ballot),
                trailing: Icon(Icons.star),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
