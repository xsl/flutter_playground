import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:fluttericon/iconic_icons.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants.dart';

class DrawerPrimaryComponent extends StatelessWidget {
  const DrawerPrimaryComponent({super.key});

  Future<void> _launchUrl(String url) async {
    if (!await launchUrl(Uri.parse(url))) {
      throw Exception('Could not launch $url');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.white,
      child: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: bPrimaryColor,
              child: Padding(
                padding: const EdgeInsets.only(top: 64.0, bottom: 24.0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: ClipOval(
                        child: Image.network(
                          'https://blmres.oss-cn-shanghai.aliyuncs.com/default_assets/images/crm/crm_square_1024.jpg',
                          width: 80,
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const Text(
                          '飞翔的珍姐',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        InkWell(
                          child: const Text(
                            'OpenAI Artificial Intelligence Inc.',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: Colors.white70,
                              fontSize: 12,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          onTap: () {
                            // Scaffold.of(context).closeDrawer();
                            // ScaffoldMessenger.of(context).showMaterialBanner(
                            //   MaterialBanner(
                            //     content: const Text('切换公司'),
                            //     actions: <Widget>[
                            //       TextButton(
                            //         onPressed: () {},
                            //         child: const Text('好的'),
                            //       )
                            //     ],
                            //   ),
                            // );
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  const SizedBox(
                    height: 8.0,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: Color(0xffefefef),
                      ),
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Iconic.user),
                    title: const Text('个人中心'),
                    onTap: () => {
                      debugPrint('Profile'),
                    },
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.notifications_active,
                    ),
                    title: const Text('我的消息'),
                    onTap: () => {
                      debugPrint('Messages'),
                    },
                  ),
                  ListTile(
                    leading: const Icon(FontAwesome.tasks),
                    title: const Text('我的待办'),
                    onTap: () => {
                      debugPrint('Tasks'),
                    },
                  ),
                  ListTile(
                    leading: const Icon(Entypo.users),
                    title: const Text('我的联系人'),
                    onTap: () => {
                      debugPrint('Contacts'),
                    },
                  ),
                  const SizedBox(
                    height: 8.0,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: Color(0xffefefef),
                      ),
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Iconic.wrench),
                    title: const Text('系统设置'),
                    onTap: () => {
                      debugPrint('Settings'),
                    },
                  ),
                  ListTile(
                    leading: const Icon(
                      Entypo.switch_icon,
                      // color: bPrimaryColor,
                    ),
                    title: const Text(
                      '切换公司',
                      style: TextStyle(
                          // color: bPrimaryColor,
                          ),
                    ),
                    onTap: () => {
                      debugPrint('Company'),
                    },
                  ),
                  ListTile(
                    leading: const Icon(Icons.logout),
                    title: const Text('退出账号'),
                    onTap: () => {
                      debugPrint('Logout'),
                    },
                  ),
                  const SizedBox(
                    height: 8.0,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: Color(0xffefefef),
                      ),
                    ),
                  ),
                  InkWell(
                    child: Image.network(
                        'https://oss.cloud.custouch.com/res/18972/%E5%AE%98%E7%BD%91-%E9%A6%96%E9%A1%B5-%E5%85%AC%E5%8F%B8%E6%88%98%E7%95%A5968x616.jpg'),
                    onTap: () {
                      // _launchUrl('https://m.baidu.com');
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
