import 'package:flutter/material.dart';

const bPrimaryColor = Color(0xff7e22ce);
const bPrimaryColorLight = Color(0xfff3e8ff);

const double defaultPadding = 16.0;

const String apiEndpoint = 'http://gw.dev.cncf.fun/adminapi';
