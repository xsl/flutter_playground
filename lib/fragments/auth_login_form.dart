import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';
import '../models/base_response_model.dart';
import '../screens/home/home_screen.dart';

class AuthLoginForm extends StatefulWidget {
  const AuthLoginForm({super.key});

  @override
  State<AuthLoginForm> createState() => _AuthLoginFormState();
}

/// 17321088716, 123456
class _AuthLoginFormState extends State<AuthLoginForm> {
  final _formKey = GlobalKey<FormState>();

  dynamic _mobile, _password;

  Future<void> onSubmit(
      BuildContext context, String mobile, String password) async {
    await Future<void>.delayed(const Duration(seconds: 1));
    SharedPreferences pref = await SharedPreferences.getInstance();
    final Dio dio = Dio();
    final response = await dio.post('$apiEndpoint/auth/login', data: {
      'username': mobile,
      'password': sha256.convert(utf8.encode(password)).toString(),
      'code': '120218',
    });
    // debugPrint(response.data.toString());
    final authLoginModel = BaseResponseModel.fromJson(response.data);

    if (context.mounted && authLoginModel.status == 0) {
      pref.setBool('isLogged', true);
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => const HomeScreen(),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            cursorColor: bPrimaryColor,
            onSaved: (mobile) {
              setState(() {
                _mobile = mobile;
              });
            },
            maxLength: 11,
            decoration: const InputDecoration(
              hintText: "您的用户名",
              prefixIcon: Padding(
                padding: EdgeInsets.all(defaultPadding),
                child: Icon(Icons.person),
              ),
            ),
            validator: (value) {
              if (value == null || value.isEmpty || value.length != 11) {
                return '请填写长度为11位的手机号码';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding),
            child: TextFormField(
              textInputAction: TextInputAction.done,
              obscureText: true,
              cursorColor: bPrimaryColor,
              onSaved: (password) {
                setState(() {
                  _password = password;
                });
              },
              decoration: const InputDecoration(
                hintText: "您的密码",
                prefixIcon: Padding(
                  padding: EdgeInsets.all(defaultPadding),
                  child: Icon(Icons.lock),
                ),
              ),
            ),
          ),
          const SizedBox(height: defaultPadding),
          Hero(
            tag: "login_btn",
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  // ScaffoldMessenger.of(context).showSnackBar(
                  //   const SnackBar(content: Text('Processing Data')),
                  // );
                  _formKey.currentState!.save();
                  _AuthLoginFormState().onSubmit(context, _mobile, _password);
                }
              },
              child: const Text(
                // "Login".toUpperCase(),
                '登陆',
                style: TextStyle(
                  fontSize: 18,
                  letterSpacing: 18,
                ),
              ),
            ),
          ),
          const SizedBox(height: defaultPadding),
          // AlreadyHaveAnAccountCheck(
          //   press: () {
          //     Navigator.push(
          //       context,
          //       MaterialPageRoute(
          //         builder: (context) {
          //           return SignUpScreen();
          //         },
          //       ),
          //     );
          //   },
          // ),
        ],
      ),
    );
  }
}
