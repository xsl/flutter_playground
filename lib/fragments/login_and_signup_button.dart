import 'package:blmapp/constants.dart';
import 'package:blmapp/screens/auth/auth_login_screen.dart';
import 'package:blmapp/screens/auth/auth_signup_screen.dart';
import 'package:flutter/material.dart';

class LoginAndSignupButton extends StatelessWidget {
  const LoginAndSignupButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Hero(
          tag: 'login_btn',
          child: ElevatedButton(
            child: const Text(
              // 'Login'.toUpperCase(),
              '登陆',
              style: TextStyle(
                fontSize: 18,
                letterSpacing: 18,
              ),
            ),
            onPressed: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return const AuthLoginScreen();
                  },
                ),
              ),
            },
          ),
        ),
        const SizedBox(
          height: defaultPadding * 2,
        ),
        ElevatedButton(
          onPressed: () => {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return const AuthSignupScreen();
                },
              ),
            )
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: bPrimaryColorLight,
            elevation: 0,
          ),
          child: const Text(
            // 'Sign Up'.toUpperCase(),
            '注册',
            style: TextStyle(
              color: Colors.black,
              fontSize: 18,
              letterSpacing: 18,
            ),
          ),
        ),
      ],
    );
  }
}
